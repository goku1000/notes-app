'use strict'
const isPublic=true;
// Read existing notes from local Storage

const getSavedNotes= () =>{
const notesJSON=localStorage.getItem('notes');
try{
    if(notesJSON!== null){
        return JSON.parse(notesJSON);
      }else{
          return [];
      }
}catch(e){
    return [];
}

if(notesJSON!== null){
  return JSON.parse(notesJSON);
}else{
    return [];
}
}

console.log(getSavedNotes());
//remove a note fromthe list 

const removeNote=(id) =>{
    console.log("Remove Note")
    const noteIndex=notes.findIndex((note) =>note.id===id)
    console.log(noteIndex);
    if(noteIndex>-1){
        notes.splice(noteIndex,1);
    }
}

//Generate the DOM structure for a note

const generateNoteDOM=(note)=> {
    const noteEl=document.createElement('a');
    const hrefLink=document.createElement('p');
    const statusEl=document.createElement('p');


    //SetUp the note title text
    if(note.title.length >0){
        hrefLink.textContent=note.title;
    }else{
        hrefLink.textContent='Unnamed Note';
    }
   
    hrefLink.classList.add('list-item__title')
    noteEl.appendChild(hrefLink);
    //Setup the link
    const hrefAttr="/edit.html#"+note.id;
    noteEl.setAttribute('href',hrefAttr)
    noteEl.classList.add('list-item');
  
    //Setup the status message
    
    statusEl.textContent=generateLastEdited(note.updatedAt)
    statusEl.classList.add('list-item__subtitle')
    noteEl.appendChild(statusEl)
 return noteEl;
}

//Sort the notes by one of the three ways
const sortNotes= (notes,sortBy)=>{
        if(sortBy ==='byEdited'){
            return notes.sort((a,b)=>{
                if(a.updatedAt<b.updatedAt){
                    return 1;
                }
                else if(a.updatedAt>b.updatedAt){
                    return -1;
                }
                else{
                    return 0;
                }
            })
        }
        else if(sortBy ==='byCreated'){
            console.log("By created called")
            return notes.sort((a,b) =>{
                if(a.createdAt<b.createdAt){
                   // console.log(`${a.title} :: ${b.title}`)
                    return 1;
                }
                else if(a.createdAt>b.createdAt){
               //     console.log(`${a.title} :: ${b.title}`)
                    return -1;
                }
                else{
                    return 0;
                }
            })
        }
        else if(sortBy ==='alphabetical'){
            return notes.sort((a,b) =>{
                if(a.title.toLowerCase()<b.title.toLowerCase()){
                    return -1;
                }
                else if(a.title.toLowerCase()>b.title.toLowerCase()){
                    return 1;
                }
                else{
                    return 0;
                }
            })
        }
        else{
            return notes;
        }

}


//Render the Application notes
const renderNotes=(notes,filter) =>{
    const notesEL=document.querySelector('#notes');
    notes=sortNotes(notes,filter.sortBy)
    const filteredNotes=notes.filter((note)=> note.title.toLowerCase().includes(filter.searchText.toLowerCase()));   
    notesEL.innerHTML=``;
   if(filteredNotes.length>0){
    filteredNotes.forEach((note,index)=>{
        const noteEl=generateNoteDOM(note);
        notesEL.appendChild(noteEl);
    })
   }else{
       const emptyMessage=document.createElement('p');
       emptyMessage.textContent='No Notes to show'
       emptyMessage.classList.add('empty-message');
       notesEL.append(emptyMessage);   
    }
}


//Save the notes to localStorage
const saveNotes=(key,value) =>{
localStorage.setItem(key,JSON.stringify(value));
}

let generateLastEdited=(timeStamp)=>` Last edited ${moment(timeStamp).fromNow().toString()}`